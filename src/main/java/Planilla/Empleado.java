package Planilla;

/**
 *
 * @author M@rlon Alvarado
 */
public class Empleado extends Persona implements IAplicarAumento {

    private int idEmpleado;
    private int salario;

    public Empleado(int idEmpleado, int salario, String cedula, String nombre) {
        super(cedula, nombre);
        this.idEmpleado = idEmpleado;
        this.salario = salario;
    }

    public int getSalario() {
        return salario;
    }

    public void setSalario(int salario) {
        this.salario = salario;
    }

    public int getIdEmpleado() {
        return idEmpleado;
    }

    @Override
    public void aplicarAumento() {
        double baseSalario = 580000, aumento = 0;
        aumento = baseSalario * 0.002;
        System.out.println("El aumento para el Empleado: " + this.getNombre() + " es de: " + aumento);
    }

}
