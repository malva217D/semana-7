package Planilla;

/**
 * 
 * @author M@rlon Alvarado
 */
public class Jefatura  extends Empleado{
    
    public Jefatura(int idEmpleado, int salario, String cedula, String nombre) {
        super(idEmpleado, salario, cedula, nombre);
    }
    
    public void aplicarAumento() {
        double baseSalario = 580000, aumento = 0, iva=0, resultado = 0;
        aumento = (baseSalario * 0.002);
        iva= aumento*0.013;
        resultado=aumento-iva;
        System.out.println("El aumento para el Jefe: " + this.getNombre() + " es de: " + resultado);
    }

}
