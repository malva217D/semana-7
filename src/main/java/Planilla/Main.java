package Planilla;

import java.util.ArrayList;


/*Aplicar en la planilla de RRHH el proceso para aplicar el aumento de salario 
a todos los empleados sin embargo a caulquira de las jefaturas el aumento 
se le debe de aplicar el descuento correspondiente al IVA. 
el aumento general es dado sobre  un 2% sobre el salario base  de ©580000 colones. 
para todos los empleados.
Imprimir la cedula, nombre, el salario, el aumento, el iva.
 */
public class Main {

    public static void main(String[] args) {

        ArrayList<Empleado> listEmpleado = new ArrayList<Empleado>();

        listEmpleado.add(new Empleado(10, 850000, "1111", "Marlon Alavarado Rojas"));
        listEmpleado.add(new Empleado(20, 1600000, "2222", "Juana Perez Calderon"));
        listEmpleado.add(new Empleado(30, 950000, "3333", "Melina Rodriguez"));
        listEmpleado.add(new Jefatura(45, 2500000, "4444", "Dagoberto Murillo Perez"));
        listEmpleado.add(new Jefatura(55, 2550000, "5555", "Yesenia Alvarez Higaldo"));

        for (Empleado e : listEmpleado) {
            e.aplicarAumento();

        }

    }

}
