/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Poliformismo;

/**
 * 
 * @author M@rlon Alvarado
 */
public class Calcular {
    
    public int sumar( int x, int y){
        return x+y;
    }
    
    public int sumar(int x, int y, int z){
        return x+y+z;
    }
    
    public int sumar (double x, int y){
        return (int)x+y;
    }
    
     public int sumar (int x, double y){
        return x+(int)y;
    }
    
    
}
