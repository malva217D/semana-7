
package Poliformismo;
// Se utiliza el  metodo con mismo nombre  pero con diferentes parametros los parametros 
//puede ser de distinto orden y de distintos tipos.

public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Calcular calc = new Calcular();
        
        // ah esto se le  conoce como sobreCarga de Metodos
        System.out.println(calc.sumar(10, 20));
        System.out.println(calc.sumar(10, 20,30));
        System.out.println(calc.sumar(10.0, 20));
        System.out.println(calc.sumar(10, 20.4));
        
        int codigo;
        String cedula;
        String nombre;
     
        
    }
    
}
