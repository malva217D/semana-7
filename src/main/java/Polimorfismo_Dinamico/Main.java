package Polimorfismo_Dinamico;

// Polimorfismo_Dinamico:
// a que metodos con el mismo nombre pero en clases diferentes pero se comportan
// de manera diferente.
/**----------------------------------------------------------
 *
 * @author M@rlon Alvarado
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
        Vehiculo automotor;
        automotor = new Vehiculo();
        automotor.mover();
        
        automotor = new Camion();
        automotor.mover();
        
    }
    
}
