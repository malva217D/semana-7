package Practica_Poliformismo;

import java.util.ArrayList;

/**
 * 
 * @author M@rlon Alvarado
 */
public class EstudiarSeresVivos {
    
   private ArrayList<SerVivo> listaSeres = new ArrayList<>();

    public EstudiarSeresVivos() {
        listaSeres.add(new Humano("Femenino"));
        listaSeres.add(new Humano("Masculino"));
        listaSeres.add(new Planta("Bonitas"));
        listaSeres.add(new Planta("Feas"));
        listaSeres.add(new Planta("Pequeñas"));
        listaSeres.add(new Animal("Domesticos"));
        listaSeres.add(new Animal("Salvajes"));
        listaSeres.add(new Animal("Hibridos"));
    }
    public void listarSeres(){
        System.out.println("Comportamiento de los seres al Nacer -- ");
        for (SerVivo sv : listaSeres) {
            sv.nacer();
            
        }
        
        System.out.println("\n-- Comportamiento de los seres al alimentarse -- ");
         for (SerVivo sv : listaSeres) {
            sv.alimentarse();
            
        }
        
        
        System.out.println("\n-- Comportamiento de los seres al Morir -- ");
         for (SerVivo sv : listaSeres) {
            sv.morir();
            
        }
        
    }
    
    
    

}
