package Practica_Poliformismo;

/**
 * 
 * @author M@rlon Alvarado
 */
public class Animal extends SerVivo {
    
   private String tipo;

    public Animal(String ptipo) {
        this.tipo = ptipo;
    }
    
     public void alimentarse() {
        System.out.println("El Animal "+ this.tipo+ " cuando encuentra su alimento ");
    }

    public void nacer() {
        System.out.println("El Animal "+ this.tipo+ " nace de la madre ");
    }

    public void morir() {
        System.out.println("El Animal "+ this.tipo+ " muere atropellado por un carro ");
    }
    
    
    
   
    
    

}
