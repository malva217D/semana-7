package Practica_Poliformismo;

/**
 *
 * @author M@rlon Alvarado
 */
abstract class SerVivo {

    protected void alimentarse() {
        System.out.println("Todos los seres vivos se Alimenta ");
    }

    protected void nacer() {
        System.out.println("Todos los seres vivos se Nacen ");
    }

    protected void morir() {
        System.out.println("Todos los seres vivos se mueren ");
    }

}
