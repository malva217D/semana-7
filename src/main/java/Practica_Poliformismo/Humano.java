package Practica_Poliformismo;

/**
 * 
 * @author M@rlon Alvarado
 */
public class Humano extends SerVivo{
    
    private String genero;

    public Humano(String genero) {
        this.genero = genero;
    }
    
    public void alimentarse() {
        System.out.println("El Humano "+ this.genero+ " trabaja para comer ");
    }

    public void nacer() {
        System.out.println("El Humano "+ this.genero+ " nace de la mama ");
    }

    public void morir() {
        System.out.println("El Humano "+ this.genero+ " muere de viejo ");
    }
    
    

}
