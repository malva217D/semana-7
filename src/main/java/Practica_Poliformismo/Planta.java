package Practica_Poliformismo;

/**
 * 
 * @author M@rlon Alvarado
 */
public class Planta extends SerVivo {
    //Atrubuto
    private String categoria;

    public Planta(String pcategoria) {
        this.categoria = pcategoria;
    }
    
    public void alimentarse() {
        System.out.println("Las Plantas "+ this.categoria+ " se alimenta del sol ");
    }

    public void nacer() {
       System.out.println("Las Plantas "+ this.categoria+ " nacen del Abono ");
    }

    public void morir() {
        System.out.println("Las Plantas"+ this.categoria+ " muere al ser cosechadas ");
    }
    

}
